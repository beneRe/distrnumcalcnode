const socket = io();
let status = {active: true, registered: false};
const email = localStorage.getItem('email');

socket.emit('register', JSON.stringify({email}));

socket.on('numberAssignment', (data) => {

    data = JSON.parse(data);

    if (!status.registered)
        socket.emit('register', JSON.stringify({email}));

    const number = data.number;
    const persScore = data.persScore;
    const overallScore = data.overallScore;
    const contribution = Math.floor((persScore / overallScore) * 100);

    $('#currNum').text(`${number}`);
    $('#persScore').text(`Personal Score: ${persScore}`);
    $('#overallScore').text(`Overall Score: ${overallScore}`);
    $('#contrBar').css('width', `${contribution}%`);

    const result = calcPrimeNumber(number);

    if (status.active)
        socket.emit('numberProcessed', JSON.stringify({result, number, email}));
    else
        localStorage.setItem('numberbuffer', JSON.stringify({result, number, email}));
});

socket.on('lock', () => {
    status.active = false;
    console.log('Client process locked by server');
    socket.emit('locked');
});

socket.on('unlock', () => {
    status.active = true;
    console.log('unlocked, sending last result to server: ');
    console.log(localStorage.getItem('numberbuffer'));
    socket.emit('numberProcessed', localStorage.getItem('numberbuffer'));
});

socket.on('registrationSuccessful', () => {
    status.registered = true;
    console.log('registered on server');
});

const calcPrimeNumber = (number) => {

    if (number === 2)
        return true;

    if (number % 2 === 0)
        return false;

    for (let i = 2; i < number; i++) {
        if (number % i === 0)
            return false;
    }

    return true;
};