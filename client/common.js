$('#newAccForm').on('submit', (e) => {

    const email = $('#newAccEmail').val();
    const password = $('#newAccPassword').val();
    const passwordconf = $('#newAccPasswordConfirm').val();

    if (password !== passwordconf) {
        $('#pwMatchErr').css('display', 'block');
        e.preventDefault();
    }

    localStorage.setItem('email', email);
});

$('#loginForm').on('submit', (e) => {

    const email = $('#loginEmail').val();

    localStorage.setItem('email', email);
});