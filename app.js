const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const favicon = require('express-favicon');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(__dirname + '/resources/icon.png'));

const router = require('./server/router');
const CC = require('./server/controllers/calcController');
const MC = require('./server/controllers/mainController');
const DBC = require('./server/controllers/dbController');

app.set('view engine', 'jade');

const server = require('http').createServer(app).listen(3000, () => {});

const dbc = new DBC.DBController();
const db = dbc.createDB();

const mc = new MC.MainController(db, __dirname);
const calcController = new CC.CalcController(db, server);

router.initRouter(app, __dirname, mc);