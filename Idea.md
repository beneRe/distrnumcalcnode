# Distributed Computing project
## Project-Idea:

Prime and Pi calculating server/client architecture!

Server:
* saves primes into a database.
* sends numbers to client to calculate if number is a prime.
* buffers numbers to know the last calculated number.
* offers socket-connection
* if server crashes it restarts at last saved prime-number.

Client:
* Calculates if number is prime
* Shows currently caluclated number
* Shows how many numbers the client caluclated
* Shows the percentage of all calculated numbers which the client calculated.