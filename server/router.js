module.exports.initRouter = function initRouter(app, rootDir, mc) {



    app.get('/', function (req, res) {
        mc.sendIndex(res);
    });

    app.get('/:site', function (req, res) {
        mc.sendSite(res, req.params.site);
    });

    app.get('/client/:filename', function (req, res) {
        mc.sendClientJS(res, req.params.filename);
    });

    app.get('/style/:filename', function (req, res) {
        mc.sendCSS(res, req.params.filename);
    });

    app.post('/acc/newAcc', function (req, res) {
        mc.createAccount(res, req.body);
    });

    app.post('/acc/login', function (req, res) {
        mc.login(res, req.body);
    });
}