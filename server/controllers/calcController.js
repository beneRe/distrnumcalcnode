const {msgLog, errLog} = require('../global/logging');
const NM = require('../models/numberModel');
const UM = require('../models/userModel');

module.exports.CalcController = class CalcController {

    constructor(db, server) {
        this.db = db;
        this.numberBuffer = [];
        this.clients = 0;
        this.lockedClients = 0;
        this.numberModel = new NM.NumberModel(db);
        this.userModel = new UM.UserModel(db);

        this.numberModel.getMaxNum(num => this.currNum = num ? num : 1);
        this.numberModel.getOverallAmount(num => this.overallScore = num ? num : 0);

        const io = require('socket.io').listen(server);
        this.initSocket(io, this);
    }

    initSocket(io, that) {

        io.on('connection', function (socket) {

            that.clients++;
            that.currNum++;

            let persScore;
            let userId;


            msgLog.info(`New User. Assigned number ${that.currNum} to him.`);

            socket.on('register', (data) => {
                try { data = JSON.parse(data); } catch (e) {}
                console.log(data);
                that.userModel.getUserId(data, id => {
                    if (id) {
                        userId = id;
                        that.numberModel.getPersonalScore(userId, result => {
                            if (result)
                                persScore = result;
                            else
                                persScore = 0;
                            socket.emit('registrationSuccessful');
                            socket.emit('numberAssignment', JSON.stringify({number: that.currNum, persScore, overallScore: that.overallScore}));
                        });
                    }
                });
            });


            socket.on('numberProcessed', (data) => {
                try { data = JSON.parse(data); } catch (e) {}
                if (data.result) {
                    persScore++;
                    that.overallScore++;
                    if (that.numberBuffer.push({number: data.number, user: userId}) === 500) {
                        io.emit('lock');
                        console.log("locking");
                    }
                }
                that.currNum++;
                socket.emit('numberAssignment', JSON.stringify({number: that.currNum, persScore, overallScore: that.overallScore}));
            });

            socket.on('locked', () => {

                that.lockedClients++;

                if (that.lockedClients === that.clients) {
                    console.log("all locked");
                    that.numberModel.saveNumbers(that.numberBuffer);
                    that.numberBuffer = [];
                    io.emit('unlock');
                    console.log("unlocked");
                    that.lockedClients = 0;
                }
            });

            socket.on('disconnect', () => {
                msgLog.info(`Client disconnected. He found ${persScore} Prime Numbers.`);
                that.clients--;
            });
        });
    }
};