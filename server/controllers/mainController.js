const path = require('path');
const UM = require('../models/userModel');

module.exports.MainController = class MainController {

    constructor(db, rootDir) {
        this.userModel = new UM.UserModel(db);
        this.clientScriptDir = { root: path.join(rootDir, '/client') };
        this.styleDir = { root: path.join(rootDir, '/style') };
    }

    sendIndex(res) {
        res.setHeader('content-type', 'text/html');
        res.render('index.jade');
    };

    sendSite(res, site) {
        res.setHeader('content-type', 'text/html');
        res.render(`${site}.jade`);
    };

    sendClientJS(res, filename) {
        res.setHeader('content-type', 'text/javascript');
        res.sendFile(filename, this.clientScriptDir);
    };

    sendCSS(res, filename) {
        res.setHeader('content-type', 'text/css');
        res.sendFile(filename, this.styleDir);
    }

    createAccount(res, data) {

        this.userModel.saveUser(data);

        if (data.nativeClient) {
            res.setHeader('content-type', 'application/json');
            res.send(JSON.stringify({result: true}));
            return;
        }

        res.setHeader('content-type', 'text/html');
        res.render('calc_main.jade');
    }

    login(res, data) {

        this.userModel.checkLogin(data, result => {
            if (data.nativeClient) {
                res.setHeader('content-type', 'application/json');
                if (result === 1)
                    return res.send(JSON.stringify({result: true}));
                else
                    return res.send(JSON.stringify({result: false}));
            } else {
                if (result === 1) {
                    res.writeHead(302, {'Location': '/calc_main'});
                }
                else {
                    res.writeHead(302, {'Location': '/'});
                }
                res.end();
            }
        });
    }
};