const sqlite = require('sqlite3').verbose();

const {msgLog, errLog} = require('../global/logging');

module.exports.DBController = class DBController {

    constructor() {
        this.dbName = './prime.db';
    }

    createDB() {
        return new sqlite.Database(this.dbName, (err) => {
            if (err)
                errLog.fatal('error connecting to db: ', err);
            msgLog.info('successfully connected to database');
        });
    }
};