const logger = require('simple-node-logger');

const msgLog = logger.createSimpleFileLogger('logs/message.log');
const errLog = logger.createSimpleLogger('logs/error.log');


msgLog.setLevel('warn');
errLog.setLevel('error');

module.exports.msgLog = msgLog;
module.exports.errLog = errLog;