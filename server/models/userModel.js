const {msgLog, errLog} = require('../global/logging');

module.exports.UserModel = class UserModel {

    constructor(db) {
        this.db = db;
    }

    saveUser(data) {
        const sql = `INSERT INTO USERS(email, password) VALUES (?, ?)`;
        this.db.run(sql, [data.email, data.password], err => {
            if (err)
                errLog.error(err);
            else
                msgLog.info("New user created: " + data.email);
        });
    };

    checkLogin(data, cb) {
        this.db.all(`SELECT COUNT(*) FROM USERS WHERE EMAIL=? AND PASSWORD=?`, [data.email, data.password], (err, rows) => {
            if (err)
                errLog.error(err);
            else {
                cb(rows[0]['COUNT(*)']);
            }
        });
    };

    getUserId(data, cb) {
        this.db.all('SELECT id FROM users WHERE email=?', [data.email], (err, rows) => {
            if (err)
                errLog.error(err);
            else {
                cb(rows[0]['id']);
            }
        });
    }
};