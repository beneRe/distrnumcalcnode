const {msgLog, errLog} = require('../global/logging');

module.exports.NumberModel = class NumberModel {

    constructor(db) {
        this.db = db;
    }

    getMaxNum(cb) {
        this.db.all('SELECT MAX(number) FROM numbersfound', [], (err, rows) => {
            if (err) errLog.error('Error fetching data: ', err);
            cb(rows[0]['MAX(number)']);
        });
    }

    getOverallAmount(cb) {
        this.db.all('SELECT MAX(counter) FROM numbersfound', [], (err, rows) => {
            if (err) errLog.error('Error fetching data: ', err);
            cb(rows[0]['MAX(counter)']);
        });
    }

    saveNumbers(numbers) {
        const db = this.db;
        const sql = "INSERT INTO numbersfound(number, user) VALUES (?, ?)";
        numbers.forEach(function (element) {
            let data = [element.number, element.user];
            db.run(sql, data, function (err) {
                if (err) errLog.warn(`Error inserting data: ${err}`);
            });
        });
    }

    getPersonalScore(userId, cb) {
        this.db.all('SELECT COUNT(number) FROM numbersfound WHERE user=?', [userId], (err, rows) => {
            if (err) errLog.error('Error fetching data: ', err);
            cb(rows[0]['COUNT(number)']);
        });
    }
};