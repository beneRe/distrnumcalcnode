DROP TABLE IF EXISTS numbersfound;
DROP TABLE IF EXISTS users;
CREATE TABLE numbersfound(counter integer primary key autoincrement, number integer unique not null, user integer not null);
CREATE TABLE users(id integer primary key autoincrement, email text not null unique, password not null);